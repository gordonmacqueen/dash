from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
from functools import wraps
from app import app, db
from app.forms import LoginForm, RegistrationForm, CourseForm, AssignCourseForm
from app.models import User, Course, CourseUser


def manager_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.is_anonymous or not current_user.manager :
            flash('Permission denied')
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return wrapper


@app.route('/')
@app.route('/index')
@login_required
def index():
    courses = [
        {
            'title': 'Learning ABC',
            'status': 'active',
            'progress': '50'
        },
        {
            'title': 'Learning 123',
            'status': 'active',
            'progress': '10'
        },
        {
            'title': 'Learning French',
            'status': 'inactive',
            'progress': '0'
        },
    ]
    return render_template('index.html',title='Home Page', courses=courses)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)
    

@app.route('/courses')
@login_required
@manager_required
def courses():
    courses = current_user.authored_courses.all()
    return render_template('courses.html', title='Course Management', courses=courses)


@app.route('/courses/add', methods=['GET', 'POST'])
@login_required
@manager_required
def add_course():
    form = CourseForm()
    if form.validate_on_submit():
        course = Course(title=form.title.data, description=form.description.data, author=current_user)
        db.session.add(course)
        db.session.commit()
        flash('New Course created')
        return redirect(url_for('courses'))
    return render_template('course.html', title='Add Course', form=form)


@app.route('/courses/<course_id>/assign', methods=['GET', 'POST'])
@login_required
@manager_required
def assign_learners(course_id):

    course = Course.query.filter_by(id=course_id).first_or_404()
    course_users = CourseUser.query.filter_by(course_id=course_id).all()

    form = AssignCourseForm()
    form.user_id.choices = [(u.id, u.username) for u in User.query.order_by('username')]

    if form.validate_on_submit():
        course_user = CourseUser(course_id=course.id, user_id=form.user_id.data)
        print(course_user)
        db.session.add(course_user)
        db.session.commit()
        flash('New Learner assigned')
        return redirect(url_for('assign_learners', course_id=course.id))
    return render_template('assign.html', title='Assign Learners', form=form, course=course, course_users=course_users)
